import { createGlobalStyle } from 'styled-components'

const GlobalStyles = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  html {
    font-size: 62.5%;
  }

  html, body, #__next {
    height: 100%;
  }

  body {
    font-family: --apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif
  }

  .container{
    width:100%;
  }

  
  @media(min-width:${props => props.theme.breakpoints.small}){
    .container{
      max-width:540px;
    }
  }

  @media(min-width:${props => props.theme.breakpoints.medium}){
    .container{
      max-width:720px;
    }
  }

  @media(min-width:${props => props.theme.breakpoints.large}){
    .container{
      max-width:960px;
    }
  }

  @media(min-width:${props => props.theme.breakpoints.xLarge}){
    .container{
      max-width:1140px;
    }
  }

  @media(min-width:${props => props.theme.breakpoints.xxLarge}){
    .container{
      max-width:1320px;
    }
  }
`

export default GlobalStyles
