import React from "react";

const CartContext = React.createContext({
  value: " "
});

export interface cartInterface{
  value: ""
}
export default function CartProvider({ children }) {
  const [cart, setCart] = React.useState<cartInterface | undefined>(undefined);

  return (
    <CartContext.Provider
      value={{
        cart,
        setCart
      }}
    >
      {children}
    </CartContext.Provider>
  );
}

export function useCart() {
  const context = React.useContext(CartContext);
  if (!context) throw new Error("useCart must be used within CartProvider");
  const { cart, setCart } = context;
  return { cart, setCart };
}


