import * as S from './styles'

import SiteHeader from '../Header/index'
import Checkout from '../Checkout/index'
import CartProvider from '../../context/cartContext'
const Main = ({
  title = 'React Avançado',
  description = 'TypeScript, ReactJS, NextJS e Styled Components'
}) => (
  <CartProvider>

  <S.Wrapper>

    <SiteHeader/>
    
    <S.container className="container">
      <Checkout/>
      
      <div className='default'>
        teste
      </div>

      {/* <S.Logo
        src="/img/logo.svg"
        alt="Imagem de um átomo e React Avançado escrito ao lado."
      />
      
      <S.Title>{title}</S.Title>
      
      <S.Description>{description}</S.Description>
      
      <S.Illustration
      src="/img/hero-illustration.svg"
        alt="Um desenvolvedor de frente para uma tela com código"
      /> */}
    </S.container>
  </S.Wrapper>
      </CartProvider>
)

export default Main
