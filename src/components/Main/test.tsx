import { screen, render } from '@testing-library/react'

import Main from '.'

describe('<Main />', () => {
  // it('should be render component', () => {
  //   const { container } = render(<Main />)

  //   expect(
  //     screen.getByRole('heading', { name: /Adicione um novo cartão de crédito/i })
  //   ).toBeInTheDocument()

  //   expect(container.firstChild).toMatchSnapshot()
  // })

  it('should render colors correctly', () => {
    const { container } = render(<Main />)
    expect(container.firstChild).toHaveStyle({
      'background-color': '#FFFFFF'
    })
  });
})
