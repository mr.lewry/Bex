import styled from 'styled-components'


export const Wrapper = styled.main`
  width: 100%;
  min-height: 100%;
  background-color: ${props => props.theme.colors.background};
  color: #fff;
  /* padding: 3rem; */
  text-align: center;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;

  @media(min-width:${props => props.theme.breakpoints.medium}){
    justify-content: start;
  }
`

export const Logo = styled.img`
  width: 25rem;
  margin-bottom: 2rem;
`

export const Title = styled.h1`
  font-size: 2.5rem;
`

export const Description = styled.h2`
  font-size: 2rem;
  font-weight: 400;
`

export const Illustration = styled.img`
  margin-top: 3rem;
  width: min(30rem, 100%);
`
export const container =  styled.section`
  padding-top:65px;
  /* width:100%; */
  background: ${props => props.theme.colors.background};
  justify-content:center;
  align-items:center;
  display:flex;
  flex-direction:column;
  flex-wrap:nowrap;

  /* justify-content:spac */

  .default{
    width:100%;
    /* height:auto; */
    min-height:285px;
    background:#f3f3f3;
  }
  .default{
    /* height:auto; */
    min-height:285px;
    background:#f3f3f3;
  }
  
  @media(min-width:${props => props.theme.breakpoints.medium}){
    display:grid;
    grid-column-gap: 15px;
    grid-template-columns: 4fr 1fr;
    align-items: start;
  }
`

