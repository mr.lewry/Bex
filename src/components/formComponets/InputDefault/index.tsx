import React,{ useEffect, useRef } from "react"
import { useField} from'@unform/core'
import * as S from './styles'

// const Input = ({ label = {} }) => (
//     <S.InputMat className="material-input">
//       <input type="text" required={true} />
//       <label htmlFor="input" className="control-label">
//         {label}
//       </label>
//       <i className="bar" />
//     </S.InputMat>
// )

// const Input = ({ label = {} }) => (
//     <S.InputMat className="material-input">
//       <input type="text" required={true} />
//       <label htmlFor="input" className="control-label">
//         {label}
//       </label>
//       <i className="bar" />
//     </S.InputMat>
// )

// const Input = ({ label, name } :{label:string, name:any}) => {
//   const inputRef = React.useRef(null);
//   const {fieldName, registerField, defaultValue, error } = useField(name);

//   React.useEffect(() => {
//     console.log(inputRef.current);

//   }, [inputRef]);
//   (
//     <S.InputMat className="material-input">
//       <input ref={inputRef} />
//       <label htmlFor="input" className="control-label">
//         {label}
//       </label>
//       <i className="bar" />
//     </S.InputMat>
// )
// }


const Input = ({ label, name , ...rest } :{label:string, name:any}) => {
    const inputRef = useRef(null);
    const {fieldName, registerField, defaultValue, error } = useField(name);
  
    useEffect(() => {
      registerField({
        name: fieldName,
        ref: inputRef.current,
        path: 'value'
      })
    }, [fieldName, registerField]);

    const clikedin  = (e) => {
      
      if(inputRef.current.value!=''|| inputRef.current.value > 0 ){
        e.target.classList.add("filled")
      }
    } 
    return(

      <S.InputMat className="material-input">
        <input ref={inputRef} { ...rest } defaultValue="" onBlur={(e) => clikedin(e)} />
        <label htmlFor="input" className="control-label">
          {label}
        </label>
        <i className="bar" />
        <span>{error}</span>
      </S.InputMat>
  )
  }

export default Input
