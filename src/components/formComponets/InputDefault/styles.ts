import styled from 'styled-components'

export const InputMat = styled.div`
    width: 100%;
  height: 70px;
  position: relative;
  font: normal normal normal 17px Verdana;
  letter-spacing: 0px;
  margin:0 10px;
  > input {
    z-index: 1;
    background-color: transparent;
    padding: 0;
    padding-left: 2px;
    width: 100%;
    position: absolute;
    bottom: 20px;
    left: 0;
    outline: none;
    box-shadow: none;
    border: none;
    border-bottom: 1px solid ${props => props.theme.colors.text};
    height: 30px;
    &:focus ~ label {
      color: ${props => props.theme.colors.primary};
    }
    &:valid{
      /* background:red; */
    }
    &:focus,
    &.filled
     {
      ~ label {
        transform: scale(0.8);
        bottom: 50px;
        left: -8px;
        font-weight: 400;
      }
    }
    &:focus ~ i {
      width: 100%;
      left: 0;
    }
  }
  > i {
    height: 2px;
    background-color: ${props => props.theme.colors.primary};
    position: absolute;
    bottom: 20px;
    width: 0;
    left: 50%;
    transition: all 0.1s ease;
  }
  > label {
    position: absolute;
    left: 0;
    bottom: 25px;
    transform: scale(0.9);
    font-size: 13px;
    transition: all 0.25s ease;
    opacity: 0.7;
    color:  ${props => props.theme.colors.text};
  }
  span{
    bottom: 0px;
    position: absolute;
    font-size: 13px;
    opacity: 0.7;
    left: 0;
    color:  ${props => props.theme.colors.primary};
  }
`
