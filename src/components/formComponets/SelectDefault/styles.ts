import styled from 'styled-components'

export const Select = styled.div`
    margin: 0;
    padding: 0;
    position: relative;
    box-sizing: border-box;
    background-color: transparent;
    border-radius: 4px;
    z-index:22; 
    margin-left:10px;
    width: 95%;
    
     select {
        width: 100%;
        max-width: 100%;
        padding: 8px 24px 8px 10px;
        border: none;
        background-color: transparent;
            -webkit-appearance: none;
            -moz-appearance: none;
        appearance: none;
        
        -webkit-appearance: none;
    -moz-appearance: none;
    -ms-appearance: none;
    appearance: none;
    outline: 0;
    box-shadow: none;
    color:${props => props.theme.colors.text};
    font: normal normal normal 12px Verdana;
    border-bottom: solid 1px ${props => props.theme.colors.text};
    }
     select:active,  select:focus {
        outline: none;
        box-shadow: none;
    }
    :after {
        content: "";
        position: absolute;
        top: 50%;
        right: 8px;
        width: 9px;
        height: 9px;
        margin-top: -7px;
        border-bottom: 1px solid ${props => props.theme.colors.primary};
        border-right: 1px solid ${props => props.theme.colors.primary};
        border-left: 1px solid transparent;
        transform: rotate(45deg);
        z-index:-1;
    }
`