import styled from 'styled-components'

export const creditCard = styled.div`
  @keyframes fade {
    0%,100% { opacity: 0 }
    50% { opacity: 1 }
  }
  position:relative;
  max-width: 100%;
  width:365px;
  height:248;
  height:225px;
  margin-top:31px;
  position:relative;
  margin-right:-31px;
  margin-bottom:-100px;
  background-color: blue;
  padding-top:104.05px;
  background-image: url('/img/cardDefault.svg');
  background-position-x: 0%;
  background-position-y: 0%;
  background-size: 90% 99%;
  background-repeat: no-repeat;
  background-attachment: initial;
  background-origin: padding-box;
  background-clip: padding-box;
  background-color: transparent;
  .cvv{
    display:none;
  }
  @media(min-width:${props => props.theme.breakpoints.medium}){

  }
  @media(min-width:${props => props.theme.breakpoints.medium}){
    max-width: 100vw;
    margin-bottom:inherit;
  }
  &.filled{
    top: -10px;
    opacity: 1;
    animation: fade .3s linear 1;
    transition: all ease-in-out .3;
    background-image: url('/img/cardFilled.svg');
  }
  &.verse{
    top: -10px;
    opacity: 1;
    animation: fade .3s linear 1;
    transition: all ease-in-out .3;
    &:after{
      animation: fade .3s linear 1;
      transition: all ease-in-out .3;
      content:' ';
      width: 100%;
      height: 100%;
      display:block;
      position:absolute;
      top:-10px;
      background-image: url('/img/cardVerse.svg');
      background-position-x: 0%;
      background-position-y: 0%;
      background-size: 90% 99%;
      background-repeat: no-repeat;
      background-attachment: initial;
      background-origin: padding-box;
      background-clip: padding-box;
      background-color: transparent;
    }
    
    .cvv{
      font: normal normal normal 18px SF sans-serif;
      letter-spacing: 2px;
      display:block;
      position:absolute;
      z-index:888;
      color:${props => props.theme.colors.textDark};
      top: 45%;
      left: 45%;
    }
  }
`
export const cardNumber = styled.span`
  position: relative;
  left:27px;
  right:25px;
  text-align: left;
  font: normal normal normal 1.8em SF;
  font: normal normal normal 1.2em sans-serif;
  letter-spacing: -0.12px;
  color: ${props => props.theme.colors.textLight};
  text-shadow: 0px 1px 2px #000000B3;
  opacity: 1;
  @media(min-width:${props => props.theme.breakpoints.small}){
    font: normal normal normal clamp(1em, 1.8em, 1.9em) sans-serif;
  }
`

export const cardDetails = styled.p`
  font: normal normal normal 1.2em sans-serif;
  margin-top:23px;
  margin-left:27px;
  margin-right:59px;
  display: flex;
  justify-content: space-between;
  flex-wrap:nowrap;
  text-shadow: 0px 1px 2px #000000B3;
  /* position: relative; */
  /* bottom:39px;
  right:24px; */
  /* text-align:left; */
`
export const  brand= styled.i`
  width:57px;
  height:17px;
  display:block;
  position:absolute;;
  top:20%;
  left:2em;
  &.visa{
    background-image: url(/img/visa.png);
    background-size:contain;
    background-repeat:no-repeat;
    background-position:center;
  }
`