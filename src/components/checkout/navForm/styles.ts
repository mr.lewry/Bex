import styled from 'styled-components'

export const NavForm = styled.nav`
    display:none;

    @media(min-width:${props => props.theme.breakpoints.medium}){
        color:black;
        width:100%;
        margin-bottom:74px;
        ol{
            display: flex;
            flex-direction: row;
            flex-wrap:nowrap;   
            color:black;
            list-style:none;
            justify-content: space-around;
            li {
                font: normal normal normal clamp(1em,1.5em,1.9em) Verdana;
                letter-spacing: -0.01px;
                color:${props=>props.theme.colors.primary};
                opacity: 1;
                i{
                    
                    font: normal normal normal 15px sans-serif;
                    color:${props=>props.theme.colors.primary};
                    margin-right:8px;
                    border: solid 1px ${props=>props.theme.colors.primary};
                    border-radius:50%;
                    padding:2px 6px;
                }
                span{
                    &:after{
                        content:' ';
                        border: 1px solid ${props=>props.theme.colors.primary};
                        border-width: 0 1.8px 1.8px 0;
                        display: inline-block;
                        padding: 4px;
                        margin-left:0.5em;
                        transform: rotate(-45deg);
                    }
                }
                &.active{
                    i{
                        position:relative;
                        &:after{
                            content:'';
                            display:inline-block;
                            position:absolute;
                            left:0px;
                            top:0px;
                            width:101%;
                            height:101%;
                            background-color:${props=>props.theme.colors.primary};
                            background-image: url('/img/checkIcon.svg');
                            background-repeat:no-repeat;
                            background-position:center center;
                            border-radius:50%;
                        }
                    }
                }
            }
        }
        
    }
    @media(min-width:${props => props.theme.breakpoints.xLarge}){
        display:block;
        color:black;
        width:100%;
        margin-bottom:74px;
        ol{
            display: flex;
            flex-direction: row;
            flex-wrap:nowrap;   
            color:black;
            list-style:none;
            justify-content: space-around;
            li {
                font: normal normal normal clamp(1em,1.5em,1.9em) Verdana;
                span{
                    &:after{
                        content:' ';
            }
        }
    }
`