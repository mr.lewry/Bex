import styled from 'styled-components'


export const containerCheckout = styled.div`
  width: 100%;
  margin:0px;
  background: ${(props) => props.theme.colors.whiteBg};
  top: 0px;
  left: 0px;
  display: flex;
  opacity: 1;
  flex-direction:column;
  & .clear{
    clear: both;
  }
  @media(min-width:${props => props.theme.breakpoints.medium}){
    display: flex;
    flex-direction:row;
  }
`
export const LeftContent = styled.div`
  display: flex;
  flex-direction:column;
  align-items: center;
  width: 100%;
  height: 100%;
  background: ${(props) => props.theme.colors.primary};
  padding-left: 40px;
  padding-right: 40px;
  padding-top: 53px;
  text-align: left;
  font: normal normal normal 13px Verdana;
  letter-spacing: -0.01px;
  margin-bottom:100px;
  
  a {
    color: ${(props) => props.theme.colors.textLight};
    text-decoration: none;
    width:100%;
  }

  @media(min-width:${props => props.theme.breakpoints.medium}){
    display: block;
    max-width: 315px;
    padding-left: 53px;
    padding-bottom: 50px;
    margin-bottom:inherit;
  }

`
export const contentTitle = styled.div`
  color:${(props) => props.theme.colors.textLight};
  display: flex;
  justify-content:space-around;
  flex-wrap: nowrap;
  margin-top:54px;

  h2{
    max-width:200px;
    font: normal normal bold 1.2em Verdana;
  }

`
export const iconCard =  styled.img`
margin-top:-5px;
  width:50px;
  height:50px;
  margin-right:15px;
  color: ${(props) => props.theme.colors.whiteBg};
`

export const RightContent = styled.div`
  display: inline-block;
  /* background: gold; */
  padding-top: 53px;
  padding-left: 35px;
  padding-right: 35px;
  padding-bottom: 50px;
  width:100%;
  height:auto;

  @media(min-width:${props => props.theme.breakpoints.medium}){
     display: inline-block;
    /* background: gold; */
    padding-top: 53px;
    padding-left: 92px;
    padding-right: 51px;
    padding-bottom: 50px;
  }
`
