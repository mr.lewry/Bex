import React ,{ useRef, useState, useContext} from'react'
import * as S from './styles'
import iconCard from '/img/iconCard.svg'
import CartContext from '../../context/cartContext'
import CreditCard from './CreditCard'
import CreditForm from './CreditForm'



const checkout = ({
  
  title = '< Alterar forma de pagamento',
  description = 'TypeScript, ReactJS, NextJS e Styled Components'
}) => {


  return(
  <S.containerCheckout>
    <S.LeftContent>
      <a href="http://" target="_blank" rel="noopener noreferrer">
        {title}
      </a>

      <S.contentTitle>

        <S.iconCard 
          src="/img/iconCard.svg"
          alt="Imagem de um átomo e React Avançado escrito ao lado."
        />

        <h2>Adicione um novo cartão de crédito</h2>

      </S.contentTitle>
      <CreditCard/>
    </S.LeftContent>

    <S.RightContent>
      <CreditForm />
    </S.RightContent>
  </S.containerCheckout>
)}

export default checkout
