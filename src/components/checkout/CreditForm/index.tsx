
import React ,{useRef, useState} from'react'
import Input from '../../formComponets/InputDefault'
import Select from '../../formComponets/SelectDefault'
import Button from '../../formComponets/ButtonDefault'
import NavForm  from '../navForm'

import { Form } from '@unform/web'
import * as Yup from 'yup'
import * as S from './styles'
import { useEffect } from 'react'
import { SubmitHandler, FormHandles } from '@unform/core'
import { useCart } from '../../../context/cartContext'

// const Input = ({ label, name } :{label:string, name:any}) => {
const CreditForm = () => {
  const formRef = useRef<FormHandles>(null)
  const  handleSubmit:SubmitHandler = async  (data:any, { reset }) =>{
    // const  handleSubmit= async  (data:any, { reset }) =>{
    try{
      const schema = Yup.object().shape({
        name:Yup.string()
        .required('O nome é obrigatório'),
        cardNumber:Yup.string()
        .min(19,'Número de cartão inválido')
        .max(19,'Número de cartão inválido')
        .required('O Número do cartão é obrigatório'),
        validate:Yup.string()
        .required('A validade é obrigatória'),
        CVV:Yup.string()
        .required('Número cvv  obrigatório'),
      })
      
      await schema.validate(data,{
        abortEarly:false,
      })
      console.log(data)
      
      reset()
    } catch(err){
      if (err instanceof Yup.ValidationError){
        console.log(err)
        const errorMessages = {};
        
        // (err.inner as any).forEach( (error: any) => { 
        (err.inner as any).forEach( (error: any) => { 
          errorMessages[error.path] = error.message;
        })
        formRef.current.setErrors(errorMessages)
      }
      else{
        console.log('err',err)
      }
    }
  }
  
  
  
  const selectOptions = [
    { value: 12, label: '12x R$1.000,00 sem juros'  },
    { value: 6, label: '6x R$2.000,00 sem juros'  }
    
  ]
  
  
  const useImageCard = async () =>{
    console.log()
    // console.log(CardNumer)
  }
  
  useImageCard()

  const { cart, setCart } = useCart();

  const savevalue = async (e) => {
    console.log(String(e.target.className))
        let value = e.target.value;
        if (String(e.target.className == 'cardNumber')){

          var foo = formRef.current.getFieldValue('cardNumber').split(" ").join("")
          
          if (foo.length > 0) {
            foo = foo.match(new RegExp('.{1,4}', 'g')).join(" ")
          }
          
          formRef.current.setFieldValue('cardNumber', foo)
        }
        if (String(e.target.className == 'validate')){

          var foo = formRef.current.getFieldValue('validate').split("/").join("")
          
          if (foo.length > 0) {
            foo = foo.match(new RegExp('.{1,2}', 'g')).join("/")
          }
          
          formRef.current.setFieldValue('validate', foo)
        }
        
        let result = e.target.value
        let creditCard = {
          creditNumber: formRef.current.getFieldValue('cardNumber') ,
          name: formRef.current.getFieldValue('name') ,
          vadidate: formRef.current.getFieldValue('validate'), 
          CVV: formRef.current.getFieldValue('CVV')

        };
        try {
          setCart({ creditCard })
        }
        catch (e) {
          console.log('Error', e);
        }
  }
  return(
    
    <>
    <NavForm />
    <Form ref={formRef} onSubmit={handleSubmit}>
    <S.ContentInput>
      <Input 
        onKeyUp={(e) => savevalue(e)} 
        name='cardNumber'
        className='cardNumber' 
        label="Número do  cartão" 
        maxLength="19"
      />
    
    </S.ContentInput>
    
    <S.ContentInput>
      <Input 
        onBlur={(e) => savevalue(e)} 
        className='nome'  
        name="name" 
        label="Nome (igual ao cartão)"
      />
    </S.ContentInput>
    
    <S.ContentInput>
    <Input 
      onChange={(e) => savevalue(e)} 
      name="validate" 
      label="Validade"  
      className="validate" 
      maxLength="5" 
    />
    <Input
      onKeyUp={(e) => savevalue(e)}
      name="CVV"
      label="CVV" 
      maxLength="4"
    />
    </S.ContentInput>
    
    <S.ContentInput>
    <Select name="country" label="Choose your country">
    {selectOptions.map(option => (
      <option key={option.value} value={option.value}>
      {option.label}
      </option>
      ))}
      </Select>
      </S.ContentInput>
      
      <S.ContentInput className="algnRIght">
      <Button label='Continuar' type="submit"/>
      </S.ContentInput>
      </Form>
      
      </>
      )
    }
    export default CreditForm