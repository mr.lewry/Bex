import styled from 'styled-components'

export const Header = styled.header`
  width: 100%;
  height: 90px;
  background-color: ${(props) => props.theme.colors.whiteBg};
`
