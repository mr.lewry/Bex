import * as S from './styles'

const SiteHeader = ({
  title = 'React Avançado',
  description = 'TypeScript, ReactJS, NextJS e Styled Components'
}) => <S.Header />

export default SiteHeader
